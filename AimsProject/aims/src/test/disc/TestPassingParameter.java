package test.disc;

import aims.disc.DigitalVideoDisc;
import aims.disc.DigitalVideoDiscWrapper;

public class TestPassingParameter {
    public static void main(String[] args) {
        DigitalVideoDisc jungleDVD = new DigitalVideoDisc("Jungle");
        DigitalVideoDisc cinderellaDVD = new DigitalVideoDisc("Cinderella");
        DigitalVideoDiscWrapper junDiscWrapper = new DigitalVideoDiscWrapper(jungleDVD);
        DigitalVideoDiscWrapper cindDiscWrapper = new DigitalVideoDiscWrapper(cinderellaDVD);

        swap(junDiscWrapper, cindDiscWrapper);
        System.out.println("Jungle DVD title: " + junDiscWrapper.getD().getTitle());
        System.out.println("Cinderella DVD title: " + cinderellaDVD.getTitle());
    }

    public static void swap(DigitalVideoDiscWrapper o1, DigitalVideoDiscWrapper o2) {
        DigitalVideoDisc tmp = o1.getD();
        o1.setD(o2.getD());
        o2.setD(tmp);
    }

    public static void changeTitle(DigitalVideoDisc dvd, String title) {
        String oldTitle = dvd.getTitle();
        dvd.setTitle(title);
        dvd = new DigitalVideoDisc(oldTitle);
    }
}
