package test.media;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import aims.disc.CompactDisc;
import aims.disc.DigitalVideoDisc;
import aims.media.Book;
import aims.media.Media;

public class TestMediaCompareTo {
    public static void main(String[] args) {
        java.util.Collection collection = new java.util.ArrayList<Media>();
        DigitalVideoDisc dvd1 = new DigitalVideoDisc("Your Highness", 1);
        DigitalVideoDisc dvd2 = new DigitalVideoDisc("Why are you running?", 3);
        DigitalVideoDisc dvd3 = new DigitalVideoDisc("Are you okay!??",2);
        collection.add(dvd1);
        collection.add(dvd2);
        collection.add(dvd3);

        Iterator iterator = collection.iterator();
        System.out.println("---------------------------------");
        System.out.println("The DVDs currently in the order are: ");
        while (iterator.hasNext()) {
            System.out.println(((DigitalVideoDisc) iterator.next()).getTitle());
        }

        Collections.sort((List) collection);
        iterator = collection.iterator();
        System.out.println("---------------------------------");
        System.out.println("The DVDs in the sorted order are: ");

        while (iterator.hasNext()) {
            System.out.println(((DigitalVideoDisc) iterator.next()).getTitle());
        }
        System.out.println("---------------------------------");

    }
}
