package test.utils;

import aims.utils.DateUtils;
import aims.utils.MyDate;

public class MyDateTest {
    public static void main(String[] args) {
        MyDate date1 = new MyDate(25, 9, 2002);
        date1.print();
        date1.print("yyyy-MMMM-dd");
        MyDate date2 = new MyDate();
        MyDate date3 = new MyDate(17, 8, 2002);
        MyDate[] orderedDates = DateUtils.sortDates(date1, date2, date3);
        System.out.println("Sorted Dates: ");
        for (int i = 0; i < orderedDates.length; i++) {
            orderedDates[i].print();
        }
    }
}
