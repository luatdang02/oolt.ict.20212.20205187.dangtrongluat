import aims.utils.MyDate;

public class App {
    public static void main(String[] args) {
        MyDate newDate = new MyDate();
        System.out.println(
                "Today is day: " + newDate.getDay() + " month: " + newDate.getMonth() + " year: " + newDate.getYear());
        MyDate dateSpecific = new MyDate(35, 12, 2002);
        System.out.println(
                "Today is day: " + dateSpecific.getDay() + " month: " + dateSpecific.getMonth() + " year: "
                        + dateSpecific.getYear());
        MyDate dateByString = new MyDate("February 18 2019");
        System.out.println(
                "Today is day: " + dateByString.getDay() + " month: " + dateByString.getMonth() + " year: "
                        + dateByString.getYear());
    }
}
