package aims;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Pattern;

import aims.disc.CompactDisc;
import aims.disc.DigitalVideoDisc;
import aims.disc.Track;
import aims.media.Book;
import aims.media.Media;
import aims.order.Order;
import test.disc.MemoryDaemon;

public class Aims {
    static ArrayList<Media> items = new ArrayList<Media>();

    public static void main(String[] args) throws Exception {
        MemoryDaemon daemon = new MemoryDaemon();
        Thread newThread = new Thread(daemon);
        newThread.setDaemon(true);
        newThread.start();
        Order anOrder = Order.createOrder();
        DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
        dvd1.setCategory("Animation");
        dvd1.setCost(19.95f);
        dvd1.setDirector("Roger Allers");
        dvd1.setLength(87);
        anOrder.addMedia(dvd1);

        DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star wars");
        dvd2.setCategory("Science fiction");
        dvd2.setCost(24.95f);
        dvd2.setDirector("George Lucas");
        dvd2.setLength(124);
        anOrder.addMedia(dvd2);

        DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
        dvd3.setCategory("Animation");
        dvd3.setCost(18.99f);
        dvd3.setDirector("John Musker");
        dvd3.setLength(87);
        anOrder.addMedia(dvd3);

        Book book1 = new Book("Sherlock holmes", "fantasy", new ArrayList<String>(Arrays.asList("james", "Dang")));
        CompactDisc disc1 = new CompactDisc("Man in the fog", "John", 140, "Horror", 20);

        Scanner scanner = new Scanner(System.in);
        items.add(dvd1);
        items.add(dvd2);
        items.add(dvd3);
        items.add(book1);
        items.add(disc1);

        showMenu(scanner);
        scanner.close();

    }

    static Order newOrder = null;

    public static void showMenu(Scanner scanner) {
        System.out.println("***************************************");
        System.out.println("Order Management Application: ");
        System.out.println("-------------------------------");
        System.out.println("1. Create new order");
        System.out.println("2. Add Item to the order");
        System.out.println("3. Delete Item by Id");
        System.out.println("4. Display the item list of orders");
        System.out.println("5. Display the item list");
        System.out.println("0. Exit");
        System.out.println("Please choose a number: 0-1-2-3-4: ");
        int choice = Integer.parseInt(scanner.nextLine());
        switch (choice) {
            case 1:
                newOrder = Order.createOrder();
                System.out.println("Order's created successfully");
                break;
            case 2:
                if (newOrder == null) {
                    System.out.println("Create an Order first");
                    break;
                }
                System.out.println("Enter the id: ");

                int id = Integer.parseInt(scanner.nextLine());
                addItemById(newOrder, id, scanner);
                break;
            case 3:
                if (newOrder == null) {
                    System.out.println("Create an Order first");
                    break;
                }
                System.out.println("Enter the id: ");
                id = Integer.parseInt(scanner.nextLine());
                deleteItemById(newOrder, id);
                break;
            case 4:
                if (newOrder == null) {
                    System.out.println("Create an Order first");
                    break;
                }
                printAllOrderList(newOrder);
                break;
            case 5:
                printAllItems();
                break;
            case 0:
                break;
            default:
                System.out.println("Invalid action, try again");
        }
        if (choice != 0)
            showMenu(scanner);
        return;
    }

    static void addItemById(Order order, int id, Scanner scanner) {
        int selectedIndex = -1;
        for (int i = 0; i < items.size(); i++) {
            if (id == items.get(i).getId()) {
                selectedIndex = i;
                break;
            }
        }
        if (selectedIndex > -1) {
            String itemType = items.get(selectedIndex).getClass().getSimpleName();
            if (itemType.equals(CompactDisc.class.getSimpleName())) {
                System.out.println("Compact disc should have track");
                System.out.println("How many tracks you want to add: ");
                int limit = Integer.parseInt(scanner.nextLine());
                CompactDisc currentItem = (CompactDisc) items.get(selectedIndex);

                for (int i = 0; i < limit; i++) {
                    scanner.useDelimiter(Pattern.compile(",\\s*"));
                    System.out.println("Enter title, length of item" + (i + 1) + " (seperated by ,):");

                    String title = scanner.next();
                    System.out.println("Title: " + title);
                    scanner.useDelimiter(Pattern.compile("\\s*"));
                    scanner.skip(",");
                    scanner.skip("\\s+");
                    int length = Integer.parseInt(scanner.nextLine());
                    System.out.println("Length: " + length);
                    Track track = new Track(title, length);
                    currentItem.addTrack(track);
                }
                System.out.println("Do you want to play the disc? (Enter 1 for yes, 2 for no): ");
                int choice = Integer.parseInt(scanner.nextLine());
                if (choice == 1) {
                    currentItem.play();
                }

            } else if (itemType.equals(DigitalVideoDisc.class.getSimpleName())) {
                System.out.println("Do you want to play the disc? (Enter 1 for yes, 2 for no): ");
                int choice = Integer.parseInt(scanner.nextLine());
                if (choice == 1) {
                    DigitalVideoDisc currentItem = (DigitalVideoDisc) items.get(selectedIndex);
                    currentItem.play();
                }
            }
            order.addMedia(items.get(selectedIndex));
        } else
            System.out.println("No such item");

    }

    static void deleteItemById(Order order, int id) {
        int index = -1;
        for (int i = 0; i < order.getItemsOrdered().size(); i++) {
            if (id == order.getItemsOrdered().get(i).getId()) {
                index = i;
                break;
            }
        }
        if (index < 0) {
            System.out.println("No such item");
            return;
        }

        order.removeMedia(order.getItemsOrdered().get(index));
    }

    static void printAllOrderList(Order order) {
        for (int i = 0; i < order.getItemsOrdered().size(); i++) {
            Media item = order.getItemsOrdered().get(i);
            System.out.println(item.getId() + ": " + item.getTitle() + " - $" + item.getCost());
        }
    }

    static void printAllItems() {
        for (int i = 0; i < items.size(); i++) {
            Media item = items.get(i);
            System.out.println(item.getId() + ": " + item.getTitle() + " - $" + item.getCost());
        }
    }
}
