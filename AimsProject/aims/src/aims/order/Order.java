package aims.order;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import aims.disc.DigitalVideoDisc;
import aims.media.Media;
import aims.utils.MyDate;

public class Order {
    public static final int MAX_NUMBERS_ORDERED = 10;
    public static final int MAX_LIMITED_ORDERS = 5;

    private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
    private int qtyOrdered = 0;
    private MyDate dateOrdered;
    private static int nbOrders = 0;
    private static int prevId = -1;
    int id;

    public MyDate getDateOrdered() {
        return dateOrdered;
    }

    public ArrayList<Media> getItemsOrdered() {
        return itemsOrdered;
    }

    public void setItemsOrdered(ArrayList<Media> itemsOrdered) {
        this.itemsOrdered = itemsOrdered;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDateOrdered(MyDate dateOrdered) {
        this.dateOrdered = dateOrdered;
    }

    private Order() {
        setDateOrdered(new MyDate());
        this.id = prevId + 1;
        genNewId();
    }

    private static void genNewId() {
        prevId = prevId + 1;
    }

    public static Order createOrder() {
        if (nbOrders < MAX_LIMITED_ORDERS) {
            nbOrders++;
            Order newOrder = new Order();
            return newOrder;
        }
        JOptionPane.showMessageDialog(null, "The number of oreders reaches its limit!", "Warning!",
                JOptionPane.WARNING_MESSAGE);
        return null;

    }

    void addMedia(Media disc) {
        if (qtyOrdered > MAX_NUMBERS_ORDERED) {
            JOptionPane.showMessageDialog(null, "The order is almost full!", "Warning!", JOptionPane.WARNING_MESSAGE);
            return;
        }
        this.itemsOrdered.add(disc);
        this.qtyOrdered++;
        JOptionPane.showMessageDialog(null, "The disc has been added!", "Success!", JOptionPane.INFORMATION_MESSAGE);
    }

    // void addDigitalVideoDisc(DigitalVideoDisc[] dvdList) {
    // int dvdLength = dvds.length;
    // if(qtyOrdered + dvdLength > MAX_NUMBERS_ORDERED) {
    // JOptionPane.showMessageDialog(null, "The discs cannot be added, not enough
    // space!", "Warning!", JOptionPane.WARNING_MESSAGE);
    // }
    // }

    public void addMedia(Media... dvds) {
        int dvdsLength = dvds.length;
        if (qtyOrdered + dvdsLength > MAX_NUMBERS_ORDERED) {
            JOptionPane.showMessageDialog(null, "The discs cannot be added, not enough space!", "Warning!",
                    JOptionPane.WARNING_MESSAGE);
            return;
        }
        for (Media dvd : dvds) {
            this.itemsOrdered.add(dvd);
            this.qtyOrdered++;
        }
        JOptionPane.showMessageDialog(null, "The discs have been added!", "Success!", JOptionPane.INFORMATION_MESSAGE);

    }

    public void removeMedia(Media disc) {
        if (qtyOrdered <= 0) {
            JOptionPane.showMessageDialog(null, "The order is empty!", "Warning!", JOptionPane.WARNING_MESSAGE);
            return;
        }
        ;
        if (itemsOrdered.remove(disc)) {
            this.qtyOrdered--;
            JOptionPane.showMessageDialog(null, "The disc has been deleted", "Success!",
                    JOptionPane.WARNING_MESSAGE);
            return;
        }
        ;
        JOptionPane.showMessageDialog(null, "No such disc found", "Fail!",
                JOptionPane.WARNING_MESSAGE);
        // Iterator itemsOrderedIterator = itemsOrdered.iterator();
        // while (itemsOrderedIterator.hasNext()) {
        // DigitalVideoDisc nextDisc = (DigitalVideoDisc) itemsOrderedIterator.next();
        // if (nextDisc.getTitle().equals(disc.getTitle())) {
        // itemsOrderedIterator.remove();
        // this.qtyOrdered--;
        // JOptionPane.showMessageDialog(null, "The disc has been deleted", "Success!",
        // JOptionPane.WARNING_MESSAGE);
        // break;
        // }
        // }

    }

    public float totalCost() {
        float totalCost = 0;
        for (int i = 0; i < itemsOrdered.size(); i++) {

            // Printing and display the elements in ArrayList
            if (itemsOrdered.get(i).getCost() > 0) {
                totalCost += itemsOrdered.get(i).getCost();
            }
        }
        return totalCost;
    }

    // public void printOrderDetails() {
    // System.out.println("Date " + dateOrdered.getDay() + " " +
    // dateOrdered.getMonth() + " " + dateOrdered.getYear());
    // System.out.println("Ordered Items");
    // itemsOrdered.forEach((o) -> {
    // System.out.println((itemsOrdered.indexOf(o) + 1) + " DVD - " + o.getTitle() +
    // " - " + o.getCategory()
    // + " - " + o.getDirector() + " - " + o.getLength() + ": $" + o.getCost());
    // });
    // System.out.println("Total cost: " + this.totalCost());
    // }

    public Media getALuckyItem() {
        int randDiscIndex = (int) Math.floor(Math.random() * qtyOrdered);
        return itemsOrdered.get(randDiscIndex);
    }
}
