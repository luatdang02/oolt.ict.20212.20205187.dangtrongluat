package aims.disc;

public interface Playable {
    public void play();
}
