package aims.disc;

import aims.media.Media;

public class Disc extends Media {
    private int length;
    private String director;

    public int getLength() {
        return length;
    }

    public String getDirector() {
        return director;
    }

    public Disc() {
    }

    public Disc(String title, String director, int length, String category, float cost) {
        super(title, category, cost);
        this.length = length;
        this.director = director;
    }

    public Disc(String title) {
        super(title);
    }

    public Disc(String title, String category) {
        super(title, category);
    }

    public Disc(String title, String category, float cost) {
        super(title, category, cost);
    }

    public Disc(String title, float cost) {
        super(title, cost);
    }
}
