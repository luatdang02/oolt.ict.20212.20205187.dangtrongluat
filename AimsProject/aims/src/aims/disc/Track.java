package aims.disc;

public class Track implements Playable {
    private String title;
    private int length;

    public String getTitle() {
        return title;
    }

    public int getLength() {
        return length;
    }

    public Track(String title, int length) {
        this.length = length;
        this.title = title;
    }

    public void play() {
        System.out.println("Playing DVD: " + this.getTitle());
        System.out.println("DVD length: " + this.getLength());
    }

    public boolean equals(Object o) {
        boolean isEqual = false;
        Track track = (Track) o;
        if (track != null && this.title == track.getTitle() && this.length == track.getLength()) {
            isEqual = true;
        }
        return isEqual;
    }

}
