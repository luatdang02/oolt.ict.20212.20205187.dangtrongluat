package aims.disc;

import aims.media.Media;

public class DigitalVideoDisc extends Disc implements Playable {
    // private int length;
    // private String director;

    public DigitalVideoDisc(String title) {
        super(title);
    }

    public DigitalVideoDisc(String category, String title) {
        super(title, category);
    }

    public DigitalVideoDisc(String title, float cost) {
        super(title, cost);
    }

    // public DigitalVideoDisc(String director, String category, String title) {
    // super(title, category);
    // this.director = director;
    // }

    public void play() {
        System.out.println("Playing DVD: " + this.getTitle());
        System.out.println("DVD length: " + this.getLength());
    }

    public DigitalVideoDisc(String director, String category, String title, int length, float cost) {
        super(title, director, length, category, cost);
    }

    public String getDirector() {
        return super.getDirector();
    }

    public void setDirector(String director) {
        // this.director = director;
    }

    public int getLength() {
        return super.getLength();
    }

    public void setLength(int length) {
        // this.length = length;
    }

    public boolean search(String title) {
        boolean contain = false;
        String[] tokenArr = title.toLowerCase().split(" ", -1);
        for (String string : tokenArr) {
            if (this.getTitle().toLowerCase().contains(string))
                contain = true;
            else {
                return false;
            }
        }
        return contain;
    }

    public int compareTo(Object obj) {
        DigitalVideoDisc discObj = (DigitalVideoDisc) obj;
        if (this.getCost() == discObj.getCost())
            return 0;
        else if (this.getCost() > discObj.getCost())
            return 1;
        else
            return -1;
    }

}