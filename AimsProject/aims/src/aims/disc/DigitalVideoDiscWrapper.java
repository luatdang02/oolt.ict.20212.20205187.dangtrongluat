package aims.disc;

public class DigitalVideoDiscWrapper {
    DigitalVideoDisc d;

    public DigitalVideoDiscWrapper(DigitalVideoDisc d) {
        this.d = d;
    }

    public DigitalVideoDisc getD() {
        return d;
    }

    public void setD(DigitalVideoDisc d) {
        this.d = d;
    }

}
