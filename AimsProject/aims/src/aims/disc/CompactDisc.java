package aims.disc;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable {
    private int length;
    private String director;

    public CompactDisc(String title, String director, int length, String category, float cost) {
        super(title, director, length, category, cost);
    }

    public CompactDisc(String title) {
        super(title);
    }

    public CompactDisc(String title, String category) {
        super(title, category);
    }

    public CompactDisc(String title, String category, float cost) {
        super(title, category, cost);
    }

    private String artist;
    private ArrayList<Track> tracks = new ArrayList<>();

    public String getArtist() {
        return artist;
    }

    public void play() {
        for (Track track : tracks) {
            System.out.println("Playing DVD: " + track.getTitle());
            System.out.println("DVD length: " + track.getLength());
        }

    }

    public void addTrack(Track track) {
        if (tracks != null && tracks.contains(track)) {
            System.out.println("The track has already been added");
            return;
        }

        tracks.add(track);
        System.out.println("The Track was added successfully");

    }

    public void removeTrack(Track track) {
        if (!tracks.contains(track)) {
            System.out.println("The track does not exist in tracks");
            return;
        }
        tracks.remove(track);
        System.out.println("The Track was removed successfully");

    }

    public int getLength() {
        int sum = 0;
        for (Track track : tracks) {
            if (track.getLength() > 0) {
                sum += track.getLength();
            }
        }
        return sum;
    }

    public ArrayList<Track> getTracks() {
        return tracks;
    }

    public int compareTo(Object obj) {
        CompactDisc discObj = (CompactDisc) obj;
        if (this.tracks.size() == discObj.getTracks().size()) {
            if (this.getLength() == discObj.getLength())
                return 0;
            else if (this.getLength() > discObj.getLength())
                return 1;
            else
                return -1;
        } else if (this.getCost() > discObj.getCost())
            return 1;
        else
            return -1;
    }

}
