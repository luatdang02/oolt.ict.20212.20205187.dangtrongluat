package aims;

import aims.disc.DigitalVideoDisc;
import aims.media.Media;
import aims.order.Order;

public class DiskTest {
    public static void main(String[] args) {
        Order anOrder = Order.createOrder();
        DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
        dvd1.setCategory("Animation");
        dvd1.setCost(19.95f);
        dvd1.setDirector("Roger Allers");
        dvd1.setLength(87);
        anOrder.addMedia(dvd1);

        DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star wars");
        dvd2.setCategory("Science fiction");
        dvd2.setCost(24.95f);
        dvd2.setDirector("George Lucas");
        dvd2.setLength(124);
        anOrder.addMedia(dvd2);

        DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
        dvd3.setCategory("Animation");
        dvd3.setCost(18.99f);
        dvd3.setDirector("John Musker");
        dvd3.setLength(87);
        anOrder.addMedia(dvd3);

        String title = "King lion";
        System.out.println(title + " is :" + dvd1.search(title));
        Media luckyItem = anOrder.getALuckyItem();
        // System.out.println(anOrder.totalCost());
        // anOrder.removeDigitalVideoDisc(dvd3);
        System.out.println("Lucky Item is: " + luckyItem.getTitle());
        // System.out.println(anOrder.totalCost());
    }
}
