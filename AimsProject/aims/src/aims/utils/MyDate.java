package aims.utils;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

public class MyDate {
    private int day;
    private int month;
    private int year;

    public MyDate() {
        LocalDate newDate = LocalDate.now();
        dateValidation(newDate.getDayOfMonth(), newDate.getMonthValue(), newDate.getYear());
        setDay(newDate.getDayOfMonth());
        setMonth(newDate.getMonthValue());
        setYear(newDate.getYear());
    }

    public MyDate(int day, int month, int year) {
        dateValidation(day, month, year);
        setDay(day);
        setMonth(month);
        setYear(year);
    }

    public MyDate(String dateString) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd yyyy");
            Date date = dateFormat.parse(dateString);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            setMonth(cal.get(Calendar.MONTH));
            setDay(cal.get(Calendar.DAY_OF_MONTH));
            setYear(cal.get(Calendar.YEAR));

        } catch (java.text.ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void dateValidation(int day, int month, int year) {
        try {
            String dateString = month + " " + day + " " + year;
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM dd yyyy");
            dateFormat.setLenient(false);
            dateFormat.parse(dateString);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();

        }
    }

    public void print() {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("M d yyyy");
            Date date = dateFormat.parse(this.month + " " + this.day + " " + this.year);
            SimpleDateFormat targetDateFormat = new SimpleDateFormat("MMMM dd yyyy");
            String targetDate = targetDateFormat.format(date);
            System.out.println(targetDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void print(String formatString) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("M d yyyy");
            Date date = dateFormat.parse(this.month + " " + this.day + " " + this.year);
            SimpleDateFormat targetDateFormat = new SimpleDateFormat(formatString);
            String targetDate = targetDateFormat.format(date);
            System.out.println(targetDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Date toDate() {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("M d yyyy");
            return dateFormat.parse(this.month + " " + this.day + " " + this.year);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
