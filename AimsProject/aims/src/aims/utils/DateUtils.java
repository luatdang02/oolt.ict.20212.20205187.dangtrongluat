package aims.utils;

import java.util.Arrays;
import java.util.Comparator;

public class DateUtils implements Comparator<MyDate> {
    public int compare(MyDate d1, MyDate d2) {
        return d1.toDate().compareTo(d2.toDate());
    }

    public static MyDate[] sortDates(MyDate... datesList) {
        Arrays.sort(datesList, new DateUtils());
        return datesList;
    }
}
