package aims.media;

public abstract class Media implements Comparable {
    private String title;
    private String category;
    private float cost;
    private static int prevId = -1;
    private int id;

    private static void genNewId() {
        prevId = prevId + 1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    protected Media() {
        this.id = prevId + 1;
        genNewId();
    }

    protected Media(String title) {
        this();
        this.title = title;
    }

    protected Media(String title, float cost) {
        this();
        this.title = title;
        this.cost = cost;
    }

    protected Media(String title, String category) {
        this(title);
        this.category = category;
    }

    protected Media(String title, String category, float cost) {
        this(title, category);
        this.cost = cost;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public boolean equals(Object o) {
        boolean isEqual = false;
        Media media = (Media) o;
        if (media != null && this.id == media.getId()) {
            isEqual = true;
        }
        return isEqual;
    }

    public int compareTo(Object obj) {
        Media discObj = (Media) obj;
        if (this.getTitle() == discObj.getTitle())
            return 0;
        else if (this.getTitle().length() > discObj.getTitle().length())
            return 1;
        else
            return -1;
    }

}
